# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [2.1.0 - 2018-12-10]
### Added
- method to get the list of roles
- method to get the data of a specific role

## [2.0.0 - 2018-05-11]
### Changed
- samply auth parent 3.0 (including samply parent POM 10.1 with Java 8 )

## Version 1.3.0

- updated documentation
- updated `Auth.DTO` dependency

## Version 1.2.0

- updated to the latest JOSE JWT library

## Version 1.1.0

- added the maven site documentation
- renamed some fields in the UserDTO object
