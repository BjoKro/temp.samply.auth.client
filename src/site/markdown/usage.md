# Library usage

## Maven

If you use Maven, you can just add the library dependency:

```
<dependency>
    <groupId>de.samply</groupId>
    <artifactId>auth-client</artifactId>
    <version>VERSION</version>
</dependency>
```

Replace `VERSION` with the library version you want to use.

## Workflows

There are three different ways to get an access token from the central Identity
Provider (IdP). This guide shows how you can use them.

### Your application is a registered client in the IdP

In this workflow you have the following values at hand:

- your client ID, a random string that is public knowledge
- your client secret, a random string that you should keep secret
- the public key from the IdP you are using. Each IdP instance must use a
  different private/public key pair.
- (optional) the random, session scoped state

In this workflow you allow certain or all users to use your client. Your client
does not need a login page, instead it uses the login page from the IdP. You
can generate the link to the Login page of the IdP using this library:

```
OAuth2Client config = new OAuth2Client();
config.setHost("https://login.mitro.dkfz.de/");
config.setClientId("your-client-id");
config.setClientSecret("your-client-secret");
config.setHostPublicKey("MII....");

String linkUrl = OAuth2ClientConfig.getRedirectUrl(config, scheme,
            serverName, port, contextPath, redirectUrl,
            Scope.OPENID);

```


Where `scheme`, `serverName`, `port` and `contextPath` are values from the request, whereas
`redirectUrl` is a relative URL in your application, e.g. "/login.xhtml".

This method will generate a URL like this:

```
https://login.mitro.dkfz.de/grant.xhtml?client_id=your-client-id&scope=openid&redirect_uri=https%3A%2F%2Fyour-host%2FsamplyLogin.xhtml
```

It is recommended to store the configuration values in a file, that is not accesible to other users, e.g.
in an XML file. You can then load the configuration file with the `JAXBUtil`:

```
OAuth2Client config = JAXBUtil.unmarshall(File file, context, OAuth2Client.class);
```

Where `file` is the file with the OAuth2Client XML, see below. You can generate a JAXBContext with `JAXBContext.newInstance(OAuth2Client.class)`.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<oAuth2Client
    xmlns="http://schema.samply.de/config/OAuth2Client"
    xmlns:xml="http://www.w3.org/XML/1998/namespace"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://schema.samply.de/config/OAuth2Client http://schema.samply.de/config/OAuth2Client.xsd">

    <host>https://the.samply.auth.url/</host>
    <hostPublicKey>
MIICI...
    </hostPublicKey>
    <clientId>your-client-id</clientId>
    <clientSecret>your-client-secret</clientSecret>
</oAuth2Client>
```


If the user clicks on the generated link he will see a login page, if he didn't login yet. After he has
logged in he will be redirected back to your web application with an additional request
parameter, the code:

```
https://your-host/samplyLogin.xhtml?code=a-random-code
```

Your application can now use this code in combination with your client ID and client secret
to get an access token, ID token and refresh token. All tokens have a limited lifetime and a different purpose:

- use the access token to access other applications via their respective REST interface
- use the ID token to identify the user, e.g. get his real name
- use the refresh token to renew your access token

You can use this library to make the finishing call:

```
AuthClient client = new AuthClient(config, "a-random-code", client);
JWTAccessToken accessToken = client.getAccessToken();
JWTIDToken idToken = client.getIDToken();
```

This method will check if the tokens are valid and so on, and return the access token
if everything is fine.

### Your client has a private key (RSA)

In this case your client always acts on behalf of exactly one user. This user is
always the same and has registered a public key in the IdP. In this case you need
to sign a random code with your private key in order to get an access token and ID token (you will never get a
refresh token using a private key).

Use the AuthClient to get a new access token:

```
AuthClient client = new AuthClient(authUrl, publicKey,
    yourPrivateKey, client);
JWTAccessToken token = client.getAccessToken();
```


### You already have a refresh token

All you need to do is to initialize the AuthClient with this refresh token
and get a new access token:

```
AuthClient client = new AuthClient(authUrl, publicKey,
    accessToken (or null), idToken (or null),
    refreshToken, client);
JWTAccessToken token = client.getAccessToken();
```

If you already have an access token you can give it to the AuthClient. Upon
requesting an access token, the client will check if the old access token is still
valid. If it is not valid any longer, it will request a new one with the
refresh token. The access token and refresh token are optional in this case.


## Use the client

You can use the client further to search for users:

```
UserListDTO userList = client.searchUser("John");
List<UserDTO> users = userList.getUsers();
```