# Samply.Auth.Client

The Samply.Auth Client library offers a client, that uses the REST interface
of the Samply.Auth application. It offers methods for all Samply.Auth workflows:

- Your application is a registered client in the Samply.Auth service
- Your application is *not* a registered client in the Samply.Auth service
- Your application already has an access token whose validity is nearing its end

An access token is valid for several hours. After this period of time, the
access token is no longer valid and should be exchanged for a new access token,
if necessary. You can use the refresh token that you got earlier to get a new
access token. This approach will only work if you used the first approach
earlier.

## Build

Use maven to build the jar:

```
mvn clean package
```

Use it as a dependency:

```xml
<dependency>
    <groupId>de.samply</groupId>
    <artifactId>auth-client</artifactId>
    <version>VERSION</version>
</dependency>
```
