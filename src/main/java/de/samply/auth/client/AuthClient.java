/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.client;

import de.samply.auth.client.jwt.*;
import de.samply.auth.rest.*;
import de.samply.auth.utils.HashUtils;
import de.samply.common.config.OAuth2Client;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.List;

/**
 * The AuthClient provides a convenient way to communicate with the Samply Auth identitiy
 * provider. There are four different constructors, each of them requires different
 * arguments.
 *
 * <pre>
 * 1. Use your client ID, secret and a code, when your application is a well-known client
 * 2. Use your private key if the public key has been registered in the identity provider
 * 3. Use your access token, ID token and refresh token if you already have them
 * 4. Use your Access token and private key, if you already have an access token
 * </pre>
 *
 *
 */
public class AuthClient {

    private static final Logger logger = LoggerFactory.getLogger(AuthClient.class);

    /**
     * The OAuth2 API URL PATH
     */
    public static final String OAUTH2_PATH = "oauth2";

    /**
     * The Auth base URL.
     */
    private final String baseUrl;

    /**
     * The Auths public key that is used to verify the signature.
     */
    private final PublicKey publicKey;

    /**
     * The Jersey Client used in this Auth Client.
     */
    private final Client client;

    /**
     * The client ID, secret and code. Optional.
     */
    private final String clientId, clientSecret, code;

    /**
     * The private key. Optional.
     */
    private final PrivateKey privateKey;

    /**
     * The access token returned from the identity provider.
     */
    private JWTAccessToken accessToken;

    /**
     * The ID token returned from the identity provider.
     */
    private JWTIDToken idToken;

    /**
     * The refresh token returned from the identity provider.
     */
    private JWTRefreshToken refreshToken;

    /**
     * Initializes this Authclient for the given OAuth2Client with the given code and HTTP client.
     * @param config the OAuth2Config instance
     * @param code the code you got from your identity provider
     * @param client the HTTP client
     */
    public AuthClient(OAuth2Client config, String code, Client client) {
        this(config.getHost(), KeyLoader.loadKey(config.getHostPublicKey()), config.getClientId(), config.getClientSecret(), code, client);
    }

    /**
     * Initializes this AuthClient with an already existing and valid access token, ID token
     * and refresh token. Use this Constructor to create a new instance in your serializable bean.
     *
     * @param baseUrl the identity provider base URL, e.g. "https://auth.osse-register.de"
     * @param publicKey the identity providers public key
     * @param accessToken your access token
     * @param idToken your ID token
     * @param refreshToken your refresh token
     * @param client the Jersey Client used for HTTP requests
     */
    public AuthClient(String baseUrl, PublicKey publicKey, JWTAccessToken accessToken, JWTIDToken idToken,
            JWTRefreshToken refreshToken, Client client) {
        this.baseUrl = baseUrl;
        this.publicKey = publicKey;
        this.client = client;
        this.clientId = this.clientSecret = this.code = null;
        this.privateKey = null;

        this.accessToken = accessToken;
        this.idToken = idToken;
        this.refreshToken = refreshToken;
    }

    /**
     *
     * @param baseUrl
     * @param clientId
     * @param clientSecret
     * @param client
     */
    public AuthClient(String baseUrl, PublicKey publicKey, String clientId, String clientSecret, Client client) {
        this.publicKey = publicKey;
        this.code = null;
        this.privateKey = null;

        this.baseUrl = baseUrl;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.client = client;
    }

    /**
     * Initializes this AuthClient with a valid access token and a private key.
     * The private key is used only, when the access token expires.
     *
     * @param baseUrl the identity provider base URL, e.g. "https://auth.osse-register.de"
     * @param publicKey the identity providers public key
     * @param accessToken your access token
     * @param privateKey your private key
     * @param client the Jersey Client used for HTTP requests
     */
    public AuthClient(String baseUrl, PublicKey publicKey, JWTAccessToken accessToken, PrivateKey privateKey,
            Client client) {
        this.baseUrl = baseUrl;
        this.publicKey = publicKey;
        this.client = client;
        this.clientId = this.clientSecret = this.code = null;
        this.privateKey = privateKey;

        this.accessToken = accessToken;
        this.idToken = null;
        this.refreshToken = null;
    }

    /**
     * Initializes this auth client for usage in a well-known OAuth2 client.
     *
     * @param baseUrl the identity provider base URL, e.g. "https://auth.osse-register.de"
     * @param publicKey the identity providers public key
     * @param clientId your client ID
     * @param clientSecret your client secret
     * @param code your code from the identity provider
     * @param client the Jersey Client used for HTTP requests
     */
    public AuthClient(String baseUrl, PublicKey publicKey, String clientId, String clientSecret, String code, Client client) {
        this.baseUrl = baseUrl;
        this.publicKey = publicKey;
        this.client = client;
        this.clientId = clientId;
        this.code = code;
        this.clientSecret = clientSecret;
        this.privateKey = null;
    }

    /**
     * Initializes this auth client for usage in a registered application with a private key.
     *
     * @param baseUrl the identity provider base URL, e.g. "https://auth.osse-register.de"
     * @param publicKey the identity providers public key
     * @param privateKey your private key
     * @param client the Jersey Client used for HTTP requests
     */
    public AuthClient(String baseUrl, PublicKey publicKey, PrivateKey privateKey, Client client) {
        this.baseUrl = baseUrl;
        this.publicKey = publicKey;
        this.client = client;
        this.clientId = this.clientSecret = this.code = null;
        this.privateKey = privateKey;
    }

    /**
     * Returns the access token for this Auth Client. Requests a new one if necessary.
     *
     * @return
     * @throws InvalidTokenException
     * @throws InvalidKeyException
     */
    public JWTAccessToken getAccessToken() throws InvalidTokenException, InvalidKeyException {
        try {
            if(accessToken == null || !accessToken.isValid()) {
                getNewAccessToken();
            }
            return accessToken;
        } catch(JWTException e) {
            logger.debug("This should never happen.");
            return null;
        }
    }

    /**
     * Returns the ID token for this Auth Client. Requests a new one if necessary.
     *
     * @return
     * @throws InvalidTokenException
     * @throws InvalidKeyException
     */
    public JWTIDToken getIDToken() throws InvalidTokenException, InvalidKeyException {
        if(code == null && refreshToken == null) {
            return null;
        } else {
            try {
                if(idToken == null) {
                    getNewAccessToken();
                }

                return idToken;
            } catch(JWTException e) {
                logger.debug("This should never happen.");
                return null;
            }
        }
    }

    /**
     * Returns a List of currently active clients.
     * @return
     */
    public ClientListDTO getClients() {
        return getClientsRequestBuilder().get(ClientListDTO.class);
    }

    /**
     * Returns the refresh token. May be null.
     * @return
     */
    public JWTRefreshToken getRefreshToken() {
        return refreshToken;
    }

    /**
     * Searches for users using the given string.
     *
     * @param input
     * @return
     * @throws InvalidKeyException
     * @throws InvalidTokenException
     */
    public UserListDTO searchUser(String input) throws InvalidTokenException, InvalidKeyException {
        return getSearchUserRequestBuilder(input).header("Authorization", getAuthorizationHeader()).get(UserListDTO.class);
    }

    /**
     * Returns a list of all locations.
     * @return
     * @throws InvalidKeyException
     * @throws InvalidTokenException
     */
    public List<LocationDTO> getLocations() throws InvalidTokenException, InvalidKeyException {
        return getLocationsRequestBuilder().header("Authorization", getAuthorizationHeader()).get(LocationListDTO.class).getLocations();
    }

    /**
     * Registers the registry in the Samply Auth identity provider.
     *
     * @param dto
     * @return
     */
    public RegistrationDTO register(RegistrationRequestDTO dto) {
        if(code != null) {
            throw new UnsupportedOperationException();
        }

        dto.setBase64EncodedPublicKey(Base64.encodeBase64String(
                KeyLoader.loadPublicRSAKey(privateKey).getEncoded()));

        return getRegisterRequestBuilder().post(Entity.json(dto), RegistrationDTO.class);
    }

    /**
     * Explicitly requests a new Access token. This is a blocking call. Use this method only if necessary.
     *
     * @return
     * @throws JWTException
     * @throws InvalidTokenException
     * @throws InvalidKeyException
     */
    public JWTAccessToken getNewAccessToken() throws JWTException, InvalidTokenException, InvalidKeyException {
        /**
         * If this is a client with a code, id and secret
         */
        logger.debug("Requesting new access token, base URL: " + baseUrl);

        if(code != null || refreshToken != null) {
            logger.debug("This is a client with an ID, a secret and a code.");
            AccessTokenRequestDTO dto = new AccessTokenRequestDTO();

            if(refreshToken == null) {
                logger.debug("No refresh token available yet");
                dto.setClientId(clientId);
                dto.setClientSecret(clientSecret);
                dto.setCode(code);
            } else {
                logger.debug("Using the refresh token");
                dto.setRefreshToken(refreshToken.getSerialized());
            }

            Builder builder = getAccessTokenRequestBuilder();
            AccessTokenDTO tokenDTO = builder.post(Entity.json(dto), AccessTokenDTO.class);

            accessToken = new JWTAccessToken(publicKey, tokenDTO.getAccessToken());
            idToken = new JWTIDToken(clientId, publicKey, tokenDTO.getIdToken());
            refreshToken = new JWTRefreshToken(publicKey, tokenDTO.getRefreshToken());

            if(!accessToken.isValid() || !idToken.isValid() || !refreshToken.isValid()) {
                logger.debug("The token we got was not valid. Throw an exception.");
                throw new InvalidTokenException();
            }

            logger.debug("ID Token: " + idToken.getSerialized());
            logger.debug("Got new valid access token using a code!");

            return this.accessToken;
        } else if(privateKey != null) {
            logger.debug("Requesting a code to sign");

            try {
                PublicKey publicKey = KeyLoader.loadPublicRSAKey(privateKey);

                KeyIdentificationDTO dto = new KeyIdentificationDTO();
                dto.setSha512Hash(HashUtils.SHA512(publicKey.getEncoded()));

                Builder builder = getSignRequestBuilder();
                SignRequestDTO post = builder.post(Entity.json(dto), SignRequestDTO.class);

                Signature signature = Signature.getInstance(post.getAlgorithm());
                signature.initSign(privateKey);
                signature.update(post.getCode().getBytes(StandardCharsets.UTF_8));

                logger.debug("Signing code:" + post.getCode());

                AccessTokenRequestDTO accessDTO = new AccessTokenRequestDTO();
                accessDTO.setCode(post.getCode());
                accessDTO.setSignature(Base64.encodeBase64String(signature.sign()));

                builder = getAccessTokenRequestBuilder();
                AccessTokenDTO tokenDTO = builder.post(Entity.json(accessDTO), AccessTokenDTO.class);

                accessToken = new JWTAccessToken(this.publicKey, tokenDTO.getAccessToken());

                /*
                 * Those tokens are not available in this workflow. Ignore them.
                 */
                idToken = null;
                refreshToken = null;

                if(!accessToken.isValid()) {
                    logger.debug("The token we got was not valid. Throw an exception.");
                    throw new InvalidTokenException();
                }

                logger.debug("Got new valid access token using a private key!");

                return this.accessToken;
            } catch (java.security.InvalidKeyException | NoSuchAlgorithmException
                    | SignatureException e) {
                logger.debug("Apparently this is not a valid RSA key!");
                throw new InvalidKeyException();
            }
        }

        throw new UnsupportedOperationException();
    }

    /**
     * Returns the authorization header, that will be used with this auth client.
     * @return
     */
    private String getAuthorizationHeader() {
        if(accessToken != null) {
            return accessToken.getHeader();
        } else {
            return "Basic " + Base64.encodeBase64String((clientId + ":" + clientSecret).getBytes());
        }
    }

    /**
     * Returns the current OAuth2 configuration.
     * @return
     */
    public OAuth2Discovery getDiscovery() {
        return getDiscoveryRequestBuilder().get(OAuth2Discovery.class);
    }

    /**
     * @return a builder for building requests to get an access token.
     */
    private Builder getAccessTokenRequestBuilder() {
        return client.target(baseUrl).path(OAUTH2_PATH).path("access_token").request(MediaType.APPLICATION_JSON);
    }

    /**
     * @return a builder for building requests to get a sign request
     */
    private Builder getSignRequestBuilder() {
        return client.target(baseUrl).path(OAUTH2_PATH).path("sign_request").request(MediaType.APPLICATION_JSON);
    }

    /**
     * @return a builder for building requests to register an application
     */
    private Builder getRegisterRequestBuilder() {
        return client.target(baseUrl).path(OAUTH2_PATH).path("register").request(MediaType.APPLICATION_JSON);
    }

    /**
     * @return a builder for building requests to get the current OAuth2 configuration.
     */
    private Builder getDiscoveryRequestBuilder() {
        return client.target(baseUrl).path(OAUTH2_PATH).path(".well-known")
                .path("openid-configuration").request(MediaType.APPLICATION_JSON);
    }

    /**
     * @param input query as a query parameter
     * @return a builder for building requests to search for users.
     */
    private Builder getSearchUserRequestBuilder(String input) {
        return client.target(baseUrl).path(OAUTH2_PATH).path("users")
                .path("search").queryParam("query", input).request(MediaType.APPLICATION_JSON);
    }

    /**
     * @return a builder for building requests to get all locations.
     */
    private Builder getLocationsRequestBuilder() {
        return client.target(baseUrl).path(OAUTH2_PATH).path("locations")
                .request(MediaType.APPLICATION_JSON);
    }

    /**
     * @return a builder for building requests to get all clients.
     */
    private Builder getClientsRequestBuilder() {
        return client.target(baseUrl).path(OAUTH2_PATH).path("clients").request(MediaType.APPLICATION_JSON);
    }

    /**
     * @return a builder for building requests to get all roles.
     */
    private Builder getRoleRequestBuilder() {
        return client.target(baseUrl).path(OAUTH2_PATH).path("roles").request(MediaType.APPLICATION_JSON);
    }

    /**
     * Returns a List of currently active roles.
     */
    public RoleListDTO getRoles() {
        return getRoleRequestBuilder().header("Authorization", getAuthorizationHeader()).get(RoleListDTO.class);
    }

    /**
     * Returns all details for a specific role
     * @param identifier The identifier of the role.
     * @return all details for a specific role
     */
    public RoleDTO getRole(String identifier){
      return getRoleRequestBuilder(identifier).header("Authorization", getAuthorizationHeader())
          .get(RoleDTO.class);
    }

    /**
     * Returns the Builder to get a specific role.
     * @param identifier The identifier of the role.
     * @return a builder for building requests for a specific role
     */
    private Builder getRoleRequestBuilder(String identifier) {
        return client.target(baseUrl).path(OAUTH2_PATH).path("roles").path(identifier)
            .request(MediaType.APPLICATION_JSON);
    }

}
